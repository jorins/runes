#!/usr/bin/env python3

import itertools
import math
import os
import shutil
import subprocess
import sys
import time

import fontforge

# Parameters
padding_h = 20.0
padding_v = 10.0
base_stroke_width = 8.0
stroke_linecap = "round"
stroke_linejoin = "round"

a = 45.0  # Angle
h = 40.0  # Horizontal step
H = 120.0  # Height
v = math.tan(a * math.pi / 180) * h  # Vertical step
W = h * 2  # Width
d = math.sqrt(v * v + h * h)  # Diagonal step
b = H - (v * 2)  # Length baseline to descender bottom

hh = h / 2
hv = v / 2
hd = d / 2
qh = h / 4
qv = v / 4
qd = d / 4

viewbox_height = H + b + padding_v
viewbox_offset_x = -padding_h / 2
viewbox_offset_y = -padding_v / 2

base_weight = 400

# Comment/uncomment to use certain weights.
# Use only 400 for quick builds/testing.
weights = {
    100: "Thin",
    200: "Extra Light",
    300: "Light",
    400: "Regular",
    500: "Medium",
    600: "Demi Bold",
    700: "Bold",
    800: "Extra Bold",
    900: "Black",
}


def _write_file(filename, contents):
    with open(filename, "w") as f:
        f.write(contents)


def _write_file_debug(filename, contents):
    print(f"Suppressed write to '{filename}' with contents:\n{contents}")


def _makedirs(directory):
    os.makedirs(directory, exist_ok=True)


def _makedirs_debug(directory):
    print(f"Suppressed creating directory '{directory}'")


def _delete(path):
    if os.path.isfile(path):
        os.remove(path)
    elif os.path.isdir(path):
        shutil.rmtree(path)
    elif os.path.exists(path):
        print(
            f"Path {path} was to be removed, but won't be because it is neither file or directory."
        )
    else:
        pass


def _delete_debug(path):
    print(f"Suppressed deleting path '{path}'")


def read_file(filename):
    with open(filename) as f:
        return f.read()


# From https://stackoverflow.com/a/312464
def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i : i + n]


def get_stroke_filename(rune, weight):
    name = "_".join([str(ord(n)) for n in rune])
    return f"out/stroke/{weight}/{name}.svg"


def get_path_filename(rune, weight):
    name = "_".join([str(ord(n)) for n in rune])
    return f"out/path/{weight}/{name}.svg"


def generate_style(weight):
    stroke_width = base_stroke_width * weight / base_weight
    return f"""path {{
    fill: none;
    stroke: black;
    stroke-width: {stroke_width};
    stroke-linecap: {stroke_linecap};
    stroke-linejoin: {stroke_linejoin};}}"""


def stroke_to_path(input_path, output_path):
    actions = ";".join(
        [
            "select-all",
            "object-stroke-to-path",
            "export-do",
        ],
    )

    try:
        subprocess.run(
            [
                "inkscape",
                "--export-type",
                "svg",
                "--export-filename",
                output_path,
                "--actions",
                actions,
                input_path,
            ]
        )
    except Exception as e:
        print(f"Failed to convert strokes to path for {filename}, reason: {e}")


def rasterise_svg(input_file, output_file=None):
    if output_file is None:
        output_file = input_file.replace("svg", "png")
    try:
        subprocess.run(
            [
                "inkscape",
                "--export-type",
                "png",
                "--export-filename",
                output_file,
                "--actions",
                "export-do",
                input_file,
            ]
        )
    except Exception as e:
        print(
            f"Failed to rasterise svg file {input_file} to {output_file}, reason: {e}"
        )


def generate_svg(path, style):
    return f"""
    <svg viewBox="{viewbox_offset_x} {viewbox_offset_y} {viewbox_width} {viewbox_height}" xmlns="http://www.w3.org/2000/svg">
        <style>{style}</style>
        <path x="0" y="0" d="{path}"></path>
    </svg>"""


def generate_preview(contents, width, height, font_size=72, y="50%"):
    return f"""
    <svg viewBox="0 0 {width} {height}" xmlns="http://www.w3.org/2000/svg">
        <style>.bg {{
            fill: white;
        }}
        text {{
            font-family: "Antifascist Magick Runes";
            font-size: {font_size}pt;
        }}
        .red {{
            fill: red;
        }}
        .ref {{
            font-family: "Noto Sans";
            font-size: 0.5em;
            fill: darkgrey;
            letter-spacing: 30;
        }}
        .w-ref {{
            font-family: "Noto Sans";
            font-size: 0.25em;
            fill: darkgrey;
            word-spacing: 15;
        }}
        </style>
        <rect x="0" y="0" width="100%" height="100%" class="bg" />
        <text x="50%" y="{y}" dominant-baseline="middle" text-anchor="middle">{contents}</text>
    </svg>"""


def generate_logo():
    return generate_preview("""<tspan dy="10%">AMR</tspan>""", 400, 400, font_size=216)


def generate_alphabet_preview():
    return generate_preview(
        """
        <tspan x="50%" dy="-1.5em">Sphinx of Black Quartz</tspan>
        <tspan x="50%" dy="1.5em">Judge my Vow</tspan>
        <tspan x="50%" dy="1.5em" class="red">♥</tspan>
    """,
        800,
        400,
    )


def generate_full_preview():
    weight_row = "".join(
        [
            f"""
        <tspan style="font-weight: {w}" letter-spacing="20">A</tspan>
    """
            for w in weights
        ]
    )
    weight_ref = f"""
        <tspan x="50%" dy="2em" class="w-ref">{" ".join([str(w) for w in weights])}</tspan>
    """

    escapes = {
        "<": "&lt;",
        ">": "&gt;",
        "&": "&amp;",
        '"': "&quot;",
        "'": "&apos;",
    }
    escaped_runes = [escapes.get(r, r) for r in runes]
    chunked_runes = list(chunks(list(escaped_runes), 12))
    rows = "".join(
        [
            f"""
        <tspan x="50%" dy="1.5em" letter-spacing="20">{"".join(row)}</tspan>
        <tspan x="50%" dy="1.5em" class="ref">{"".join(row)}</tspan>"""
            for row in chunked_runes
        ]
    )
    canvasHeight = (1 + len(chunked_runes)) * 220
    return generate_preview(weight_row + weight_ref + rows, 800, canvasHeight, y="1em")


def generate_footer():
    return generate_preview(
        """
        <tspan x="50%" dy="-0.75em">The only good Nazi</tspan>
        <tspan x="50%" dy="1.5em">is one that is dead</tspan>
    """,
        800,
        300,
    )


def add_glyph(font, name, source_file):
    glyph = font.createChar(ord(name), name)
    glyph.importOutlines(source_file)
    glyph.left_side_bearing = int(padding_h / 2)
    glyph.right_side_bearing = int(padding_h / 2)
    glyph.round()


def add_reference(font, source, destination):
    glyph = font.createChar(ord(destination), destination)
    glyph.addReference(source)
    glyph.left_side_bearing = int(padding_h / 2)
    glyph.right_side_bearing = int(padding_h / 2)
    glyph.round()


def homoglyph(glyph):
    """Shorthand for a homoglyph reference, for legibility"""
    return f"#HOMOGLYPH {glyph}"


runes = {
    # Comment out all but one for quick testing of the inkscape conversion pipeline.
    "!": f"M {hh} 0 l 0 {H-v*1.5} m 0 {hv} l {hh} {hv} l {-hh} {hv} {-hh} {-hv} {hh} {-hv}",
    '"': f"M {qh} 0 l 0 {hv} m {hh} 0 l 0 {-hv}",
    "#": f"M {qh} {H/2-hv} l 0 {v*2} m {hh} 0 l 0 {-v*2} M {h} {H/2-hv} l {-h} {v} m 0 {v} l {h} {-v}",
    "$": f"M {h} {hv} l {-hh} {-hv} l {-hh} {hv} L 0 {H/2-hv} l {h} {v} L {h} {H-hv} l {-hh} {hv} l {-hh} {-hv} M {hh} 0 l 0 {H}",
    "%": f"M {hh} 0 l {hh} {hv} l {-hh} {hv} l {-hh} {-hv} z M 0 {H/2+v} l {h*2} {-v*2} M {W-hh} {H} l {-hh} {-hv} l {hh} {-hv} l {hh} {hv} l {-hh} {hv}",
    "&": f"M {h*1.25} {H} l {-h*1.25} {-v*1.25} l {hh} {-hv} l {hh} {hv} l {-h} {v} l {hh} {hv} l {h*0.75} {-v*0.75}",
    "'": f"M {hh} 0 l 0 {hv}",
    "(": f"M {h} 0 l {-hh} {hv} L {hh} {H-hv} l {hh} {hv}",
    ")": f"M 0 0 l {hh} {hv} L {hh} {H-hv} l {-hh} {hv}",
    "*": f"M {hh} {H/2-d/2} l {0} {d} M 0 {H/2-hv} l {h} {v} M 0 {H/2+hv} l {h} {-v}",
    "+": f"M 0 {H/2} l {h} 0 M {hh} {H/2-hv} l 0 {v}",
    ",": f"M {hh} {H-v} l {hh} {hv} l {-h} {v} l 0 {-v} Z",
    "-": f"M 0 {H/2} l {h} 0",
    ".": f"M {hh} {H-v} l {hh} {hv} l {-hh} {hv} l {-hh} {-hv} Z",
    "/": f"M {W} {H/2-v} l {-h*2} {v*2}",
    "0": f"M 0 {hv} l {hh} {-hv} l {hh} {hv} L {h} {H-hv} l {-hh} {hv} l {-hh} {-hv} Z M 0 {H/2+hv} l {h} {-v}",
    "1": f"M {hh} {H} l 0 {-H} l {-hh} {hv}",
    "2": f"M {h} {H} l {-h} 0 l 0 {-hd} l {h} {-v} L {h} {hv} l {-hh} {-hv} l {-hh} {hv}",
    "3": f"M 0 {hv} l {hh} {-hv} l {hh} {hv} L {h} {H/2 - hv} l {-hh} {hv} l {hh} {hv} L {h} {H - hv} l {-hh} {hv} l {-hh} {-hv}",
    "4": f"M {h} {H} l 0 {-2*v} M {h} 0 l {-h} {v*1.5} L 0 {H-v} l {h*1.25} 0",
    "5": f"M {h} 0 l {-h} 0 L 0 {H/2} l {hh} {-hv} l {hh} {hv} L {h} {H-hv} l {-hh} {hv} l {-hh} {-hv}",
    "6": f"M {h} {hv} l {-hh} {-hv} l {-hh} {hv} L 0 {H-hv} l {hh} {hv} l {hh} {-hv} l 0 {-hd} l {-hh} {-hv} l {-hh} {hv}",
    "7": f"M 0 0 L {h} 0 L 0 {H}",
    "8": f"M 0 {hv} l {hh} {-hv} l {hh} {hv} l 0 {hd} l {-hh} {hv} l {-hh} {-hv} Z M 0 {H-hv} l {hh} {hv} l {hh} {-hv} l 0 {-hd} l {-hh} {-hv} l {-hh} {hv} Z",
    "9": f"M 0 {H-hv} l {hh} {hv} l {hh} {-hv} L {h} {hv} l {-hh} {-hv} l {-hh} {hv} l 0 {hd} l {hh} {hv} l {hh} {-hv}",
    ":": f"M {hh} {H-v} l {hh} {hv} l {-hh} {hv} l {-hh} {-hv} Z l {-hh} {-hv} l {hh} {-hv} l {hh} {hv} Z",
    ";": f"M {hh} {H-v} l {hh} {hv} l {-h} {v} l 0 {-v} Z l {-hh} {-hv} l {hh} {-hv} l {hh} {hv} Z",
    "<": f"M {h} {H/2-v} l {-h} {v} l {h} {v}",
    "=": f"M 0 {H/2-qv} l {h} 0 m 0 {hv} l {-h} 0",
    ">": f"M 0 {H/2-v} l {h} {v} l {-h} {v}",
    "?": f"M 0 {hv} l {hh} {-hv} l {hh} {hv} l {-hh} {hv} L {hh} {H-v*1.5} m 0 {hv} l {hh} {hv} l {-hh} {hv} l {-hh} {-hv} l {hh} {-hv}",
    "@": f"M {W} {H+b-v} l {-h} {v} l {-h} {-v} L 0 {b} l {h} {-v} l {h} {v} Z l {-hh} {-hv} L {h*1.5} {b+hv} l {-hh} {-hv} l {-hh} {hv} L {hh} {H-hv} l {hh} {hv} l {hh} {-hv}",
    "A": f"M 0 {H} L 0 {hv} l {hh} {-hv} l {hh} {hv} L {h} {H} M 0 {H/2} l {hh} {hv} l {hh} {-hv}",
    "B": f"M 0 {2*v} l {h} {-v} l {-h} {-v} L 0 {H} l {h} {-v} l {-h} {-v}",
    "C": f"M {h} {hv} l {-hh} {-hv} l {-hh} {hv} L 0 {H-hv} l {hh} {hv} l {hh} {-hv}",
    "D": f"M 0 {H} l {h} {-v} L {h} {v} l {-h} {-v} Z",
    "E": f"M {h} {hv} l {-hh} {-hv} l {-hh} {hv} L 0 {H-hv} l {hh} {hv} l {hh} {-hv} M {-qh} {H/2+qv} l {hh} {-hv}",
    "F": f"M {h} {hv} l {-hh} {-hv} l {-hh} {hv} L 0 {H} M {-qh} {(H+v)/2} l {hh} {-hv}",
    "G": f"M {h} {hv} l {-hh} {-hv} l {-hh} {hv} L 0 {H-hv} l {hh} {hv} l {hh} {-hv} l 0 {-d/2} l {-qh} {-qv}",
    "H": f"M 0 0 L 0 {H} M 0 {(H+v)/2} l {h} {-v} M 0 {(H-v)/2} l {h} {v} M {h} 0 L {h} {H}",
    "I": f"M {hh} 0 l 0 {H}",
    "J": f"M {hh} {hv} l {hh} {-hv} l 0 {H-hv} l {-hh} {hv} l {-hh} {-hv}",
    "K": f"M {h} 0 l {-h} {v} l {h} {v} L {h} {H} l {-hh} {-hv} M 0 0 L 0 {H}",
    "L": f"M 0 0 L 0 {H} l {h} {-v}",
    "M": f"M 0 {H} L 0 0 l {h} {v} l {hh} {hv} l {-hh} {hv} {-hh} {-hv} {hh} {-hv} l {h} {-v} l 0 {H}",
    "N": f"M 0 {H} L 0 {H/2-v} l {h} {v} M {h} 0 L {h} {H/2+v} l {-h} {-v}",
    "O": f"M 0 {hv} l {hh} {-hv} l {hh} {hv} L {h} {H-hv} l {-hh} {hv} l {-hh} {-hv} Z M {hh} {H/2-d/4} l 0 {d/2}",
    "P": f"M 0 {H} L 0 0 l {h} {v} l {-h} {v}",
    "Q": f"M 0 {hv} l {hh} {-hv} l {hh} {hv} L {h} {H-hv} l {-hh} {hv} l {-hh} {-hv} Z M {h} {H} l {-hh} {-hv}",
    "R": f"M 0 {H} L 0 0 l {h} {v} l {-h} {v} M {h} {H} l {-h} {-v}",
    "S": f"M {h} {hv} l {-hh} {-hv} l {-hh} {hv} L 0 {H/2-hv} l {h} {v} L {h} {H-hv} l {-hh} {hv} l {-hh} {-hv}",
    "T": f"M 0 {hv} l {hh} {-hv} l {hh} {hv} m {-hh} {-hv} L {hh} {H}",
    "U": f"M 0 0 l 0 {H} l {h} {-v} M {h} 0 l 0 {H} l {-h} {-v}",
    "V": f"M 0 0 L 0 {H-hv} l {hh} {hv} l {hh} {-hv} L {h} 0",
    "W": f"M 0 0 L 0 {H-hv} l {hh} {hv} l {hh} {-hv} L {h} {v} M {W} 0 L {W} {H-hv} l {-hh} {hv} l {-hh} {-hv}",
    "X": f"M 0 0 L 0 {H/2-hv} l {h} {v} L {h} {H} M 0 {H} L 0 {H/2+hv} l {h} {-v} L {h} 0",
    "Y": f"M 0 0 L 0 {H/2-hv} l {hh} {hv} l {hh} {-hv} L {h} 0 M {hh} {H/2} L {hh} {H}",
    "Z": f"M 0 0 l {h} {v} l {-h} {v} l {h} {v}",
    "[": f"M {h} 0 l {-hh} 0 l 0 {H} l {hh} 0",
    "\\": f"M 0 {H/2-v} l {h*2} {v*2}",
    "]": f"M 0 0 l {hh} 0 l 0 {H} l {-hh} 0",
    "^": f"M 0 {hv} l {hh} {-hv} l {hh} {hv}",
    "_": f"M 0 {H} l {h} 0",
    "`": f"M {qh} 0 l {hh} {hv}",
    "a": f"M {h} {H} l 0 {-hv} L {h} {b+hv} l {-hh} {-hv} l {-hh} {hv} L 0 {H-hv} l {hh} {hv} l {hh} {-hv}",
    "b": f"M 0 0 L 0 {H} l {h} {-v} l {-h} {-v}",
    "c": f"M {h} {b+hv} l {-hh} {-hv} l {-hh} {hv} L 0 {H-hv} l {hh} {hv} l {hh} {-hv}",
    "d": f"M {h} 0 L {h} {H} l {-h} {-v} l {h} {-v}",
    "e": f"M {h} {H-hv} l {-hh} {hv} l {-hh} {-hv} L 0 {b+hv} l {hh} {-hv} l {hh} {hv} l {-h} {v}",
    "f": f"M 0 {H} l {hh} {-hv} l 0 {-H+v} l {hh} {-hv} M {hh} {H/2} m {-qh} {qv} l {hh} {-hv}",
    "g": f"M {h} {H-hv} L {h} {b+hv} l {-hh} {-hv} l {-hh} {hv} L 0 {H-hv} l {hh} {hv} l {hh} {-hv} l 0 {b} l {-hh} {hv} l {-hh} {-hv}",
    "h": f"M 0 {H} L 0 0 m 0 {b} m 0 {hv} l {hh} {-hv} l {hh} {hv} L {h} {H}",
    "i": f"M {hh} 0 l 0 {b/2} m 0 {b/2} l 0 {H-b}",
    "j": f"M {hh} 0 l 0 {b/2} m 0 {b/2} l 0 {H-b+hv} l {-hh} {hv}",
    "k": f"M 0 0 L 0 {H} M {h} {H} l {-h} {-v} M {h} {b} l {-h} {v}",
    "l": f"M {hh} 0 l 0 {H} l {hh} {-hv}",
    "m": f"M 0 {H} L 0 {b} m 0 {hv} l {hh} {-hv} l {hh} {hv} L {h} {H} M {h} {b+hv} l {hh} {-hv} l {hh} {hv} L {W} {H}",
    "n": f"M 0 {H} L 0 {b} m 0 {hv} l {hh} {-hv} l {hh} {hv} L {h} {H}",
    "o": f"M 0 {b+hv} l {hh} {-hv} l {hh} {hv} L {h} {H-hv} l {-hh} {hv} l {-hh} {-hv} Z",
    "p": f"M 0 {H+b} L 0 {b} l {h} {v} l {-h} {v}",
    "q": f"M {h} {H+b} L {h} {b} l {-h} {v} l {h} {v}",
    "r": f"M 0 {b} L 0 {H} M {h} {b} l {-h} {v}",
    "s": f"M {h} {b+hv} l {-hh} {-hv} l {-hh} {hv} l {h} {v} l {-hh} {hv} l {-hh} {-hv}",
    "t": f"M 0 {b+v} l {hh} {-hv} l {hh} {hv} M {hh} {b} L {hh} {H} l {hh} {-hv}",
    "u": f"M 0 {b} L 0 {H-hv} l {hh} {hv} l {hh} {-hv} M {h} {H} L {h} {b}",
    "v": f"M 0 {b} L 0 {H-hv} l {hh} {hv} l {hh} {-hv} L {h} {b}",
    "w": f"M 0 {b} L 0 {H-hv} l {hh} {hv} l {hh} {-hv} L {h} {b+hv} M {W} {b} L {W} {H-hv} l {-hh} {hv} l {-hh} {-hv}",
    "x": f"M 0 {b} l {hh} {hv} l {hh} {-hv} m {-hh} {hv} L {hh} {H-hv} M 0 {H} l {hh} {-hv} l {hh} {hv}",
    "y": f"M 0 {b} L 0 {H-hv} l {hh} {hv} l {hh} {-hv} M {h} {b} L {h} {H+b-hv} l {-hh} {hv} l {-hh} {-hv}",
    "z": f"M 0 {b} l {h*0.75} {v*0.75} l {-hh} {hv} L {h} {H}",
    "{": f"M {h} 0 l {-hh} {hv} L {hh} {H-hv} l {hh} {hv} M {hh} {H/2-hv} l {-hh} {hv} l {hh} {hv}",
    "|": homoglyph("I"),
    "}": f"M 0 0 l {hh} {hv} L {hh} {H-hv} l {-hh} {hv} M {hh} {H/2-hv} l {hh} {hv} l {-hh} {hv}",
    "~": f"M 0 {H/2} l {qh} {-qv} l {hh} {hv} l {qh} {-qv}",
    "¡": f"M {hh} {H} L {hh} {v*1.5} m 0 {-hv} l {hh} {-hv} l {-hh} {-hv} {-hh} {hv} {hh} {hv}",
    "¢": f"M {h} {b+hv} l {-hh} {-hv} l {-hh} {hv} L 0 {H-hv} l {hh} {hv} l {hh} {-hv} M {hh} {b/2} l 0 {H}",
    "£": f"M {h} {hv} l {-hh} {-hv} l {-hh} {hv} L 0 {H-hv} l {h} {v} M {-qh} {H/2+qv} l {hh} {-hv}",
    "¤": f"M {hh} {H/2-hv} l {hh} {hv} l {-hh} {hv} l {-hv} {-hh} l {hh} {-hv} M 0 {H/2-hv} l {qh} {qv} m {hh} 0 l {qh} {-qv} m 0 {v} l {-qh} {-qv} m {-hh} 0 l {-qh} {qv}",
    "¥": f"M 0 0 L 0 {H/2-hv} l {hh} {hv} l {hh} {-hv} L {h} 0 M {hh} {H/2} L {hh} {H} M {qh} {H-qv} l {hh} {-hv} m 0 {-hv} l {-hh} {hv}",
    "¦": f"M {hh} 0 l 0 {H/2-hv} m 0 {v} L {hh} {H}",
    "§": f"M {h} {hv} l {-hh} {-hv} l {-hh} {hv} l {hh} {hv} M {hh} {H/2-hv} l {hh} {hv} l {-hh} {hv} l {-hh} {-hv} l {hh} {-hv} M 0 {H-hv} l {hh} {hv} l {hh} {-hv} l {-hh} {-hv}",
    "¨": f"M {hh} 0 l {h} {v} l {hh} {-hv} l {-hh} {-hv} l {-h} {v} l {-hh} {-hv} z",
    "©": f"M {W} {H+b-v} l {-h} {v} l {-h} {-v} L 0 {b} l {h} {-v} l {h} {v} Z M {h*1.5} {b+hv} l {-hh} {-hv} l {-hh} {hv} L {hh} {H-hv} l {hh} {hv} l {hh} {-hv}",
    "«": f"M {hh} {H/2-hv} l {-hh} {hv} l {hh} {hv} m {hh} 0 l {-hh} {-hv} l {hh} {-hv}",
    "¬": f"M 0 {H/2} l {h} 0 l 0 {hv}",
    "®": f"M {W} {H+b-v} l {-h} {v} l {-h} {-v} L 0 {b} l {h} {-v} l {h} {v} Z M {hh} {H+hv} l 0 {-H} l {h} {v} l {-h} {v} l {h} {v}",
    "¯": f"M 0 0 l {h} 0",
    "°": f"M {hh} 0 l {hh} {hv} l {-hh} {hv} l {-hh} {-hv} l {hh} {-hv}",
    "±": f"M 0 {H/2} l {h} 0 M {hh} {H/2-hv} l 0 {v} M 0 {H/2+v} l {h} 0",
    "´": f"M {h*0.75} 0 l {-hh} {hv}",
    "µ": f"M 0 {H+b} L 0 {b} M 0 {H-hv} l {hh} {hv} l {hh} {-hv} M {h} {H} L {h} {b}",
    "·": f"M 0 {H/2} l {hh} {-hv} l {hh} {hv} l {-hh} {hv} z",
    "»": f"M 0 {H/2-hv} l {hh} {hv} l {-hh} {hv} m {hh} 0 l {hh} {-hv} l {-hh} {-hv}",
    "¿": f"M 0 {H-hv} l {hh} {hv} l {hh} {-hv} l {-hh} {-hv} L {hh} {v*1.5} m 0 {-hv} l {hh} {-hv} l {-hh} {-hv} l {-hh} {hv} l {hh} {hv}",
    "Á": f"M 0 {H} L 0 {hv} l {hh} {-hv} l {hh} {hv} L {h} {H} M 0 {H/2} l {hh} {hv} l {hh} {-hv} M {qh} {-qv} l {hh} {-hv}",
    "Ä": f"M 0 {H} L 0 {hv} l {hh} {-hv} l {hh} {hv} L {h} {H} M 0 {H/2} l {hh} {hv} l {hh} {-hv} M {qh} {-qv} l 0 {-hv} m {hh} 0 l 0 {hv}",
    "Å": f"M 0 {H} L 0 {hv} l {hh} {-hv} l {hh} {hv} L {h} {H} M 0 {H/2} l {hh} {hv} l {hh} {-hv} M {hh} {-qv} l {-hh} {-hv} l {hh} {-hv} l {hh} {hv} l {-hh} {hv}",
    "Æ": f"M 0 {H} L 0 {hv} l {hh} {-hv} l {hh} {hv} l {hh} {-hv} l {hh} {hv} m {-h} 0 L {h} {H} m 0 {-hv} l {hh} {hv} l {hh} {-hv} M 0 {H/2} l {hh} {hv} l {h*0.75} {-v*0.75}",
    "É": f"M {h} {hv} l {-hh} {-hv} l {-hh} {hv} L 0 {H-hv} l {hh} {hv} l {hh} {-hv} M {-qh} {H/2+qv} l {hh} {-hv} M {qh} {-qv} l {hh} {-hv}",
    "Í": f"M {hh} 0 l 0 {H} M {qh} {-qv} l {hh} {-hv}",
    "Ð": f"M 0 {H} l {h} {-v} L {h} {v} l {-h} {-v} Z M {-qh} {H/2+qv} l {hh} {-hv}",
    "Ó": f"M 0 {hv} l {hh} {-hv} l {hh} {hv} L {h} {H-hv} l {-hh} {hv} l {-hh} {-hv} Z M {hh} {H/2-d/4} l 0 {d/2} M {qh} {-qv} l {hh} {-hv}",
    "Ö": f"M 0 {hv} l {hh} {-hv} l {hh} {hv} L {h} {H-hv} l {-hh} {hv} l {-hh} {-hv} Z M {hh} {H/2-d/4} l 0 {d/2} M {qh} {-qv} l 0 {-hv} m {hh} 0 l 0 {hv}",
    "×": f"M 0 {H/2-hv} l {h} {v} M 0 {H/2+hv} l {h} {-v}",
    "Ø": homoglyph("0"),
    "Ú": f"M 0 0 L 0 {H-v} l {h} {v} L {h} 0 M {qh} {-qv} l {hh} {-hv}",
    "Ý": f"M 0 0 L 0 {H/2-hv} l {hh} {hv} l {hh} {-hv} L {h} 0 M {hh} {H/2} L {hh} {H} M {qh} {-qv} l {hh} {-hv}",
    "Þ": f"M 0 0 L 0 {H} M 0 {H/2-v} l {h} {v} l {-h} {v}",
    "á": f"M {h} {H} l 0 {-hv} L {h} {b+hv} l {-hh} {-hv} l {-hh} {hv} L 0 {H-hv} l {hh} {hv} l {hh} {-hv} M {h*0.75} 0 l {-hh} {hv}",
    "ä": f"M {h} {H} l 0 {-hv} L {h} {b+hv} l {-hh} {-hv} l {-hh} {hv} L 0 {H-hv} l {hh} {hv} l {hh} {-hv} M {qh} 0 l 0 {hv} m {hh} 0 l 0 {-hv}",
    "å": f"M {h} {H} l 0 {-hv} L {h} {b+hv} l {-hh} {-hv} l {-hh} {hv} L 0 {H-hv} l {hh} {hv} l {hh} {-hv} M {hh} 0 l {hh} {hv} l {-hh} {hv} l {-hh} {-hv} l {hh} {-hv}",
    "æ": f"M 0 {b+hv} l {hh} {-hv} l {hh} {hv} l {hh} {-hv} l {hh} {hv} l {-h} {v} m 0 {-v} L {h} {H} M 0 {b+hv} L 0 {H-hv} l {hh} {hv} l {hh} {-hv} l {hh} {hv} l {hh} {-hv}",
    "é": f"M {h} {H-hv} l {-hh} {hv} l {-hh} {-hv} L 0 {b+hv} l {hh} {-hv} l {hh} {hv} l {-h} {v} M {h*0.75} 0 l {-hh} {hv}",
    "í": f"M {h*0.75} 0 l {-hh} {hv} M {hh} {b} L {hh} {H}",
    "ð": f"M {hh} {b} l {-hh} {hv} L 0 {H-hv} l {hh} {hv} l {hh} {-hv} L {h} {b+hv} l {-h} {-v} m 0 {hv} l {hh} {-hv}",
    "ó": f"M 0 {b+hv} l {hh} {-hv} l {hh} {hv} L {h} {H-hv} l {-hh} {hv} l {-hh} {-hv} Z M {h*0.75} 0 l {-hh} {hv}",
    "ö": f"M 0 {b+hv} l {hh} {-hv} l {hh} {hv} L {h} {H-hv} l {-hh} {hv} l {-hh} {-hv} Z M {qh} 0 l 0 {hv} m {hh} 0 l 0 {-hv}",
    "ø": f"M 0 {b+hv} l {hh} {-hv} l {hh} {hv} L {h} {H-hv} l {-hh} {hv} l {-hh} {-hv} Z M 0 {(H+b+v)/2} l {h} {-v}",
    "ú": f"M 0 {b} L 0 {H-hv} l {hh} {hv} l {hh} {-hv} M {h} {H} L {h} {b} M {h*0.75} 0 l {-hh} {hv}",
    "ý": f"M 0 {b} L 0 {H-hv} l {hh} {hv} l {hh} {-hv} M {h} {b} L {h} {H+b-hv} l {-hh} {hv} l {-hh} {-hv} M {h*0.75} 0 l {-hh} {hv}",
    "þ": f"M 0 {b} L 0 {H+b} M 0 {b+H/2-v} l {h} {v} l {-h} {v}",
    "ᚠ": f"M 0 0 l 0 {H} M {hh} 0 l {-hh} {hv} M {h} 0 l {-h} {v}",
    "ᚡ": f"M 0 0 l 0 {H} M {hh} 0 l {-hh} {hv} M {h} 0 l {-h} {v} M {qh} 0 l 0 {qv}",
    "ᚢ": f"M 0 {H} l 0 {-H} l {h} {v} L {h} {H}",
    "ᚣ": f"M 0 {H} l 0 {-v*2} l {h*2} {v*2} M {h} {H} l 0 {-hv}",
    "ᚤ": f"M 0 {H} l 0 {-H} l {h} {v} L {h} {H} M {hh} {v} l 0 {hv}",
    "ᚥ": f"M 0 {H} l 0 {-H} l {h} {v} L {h} {H} M 0 {v} l {hh} {hv} L {hh} {H}",
    "ᚦ": f"M 0 0 l 0 {H} M 0 {H/2-v} l {h} {v} l {-h} {v}",
    "ᚧ": f"M 0 0 l 0 {H} M 0 {H/2-v} l {h} {v} l {-h} {v} M {h/3} {H/2-qv} l 0 {hv}",
    "ᚨ": f"M 0 {H} L 0 0 l {hh} {hv} m {-hh} 0 l {hh} {hv}",
    "ᚩ": f"M 0 {H} L 0 0 l {hh} {hv} l {hh} {-hv} M 0 {hv} l {hh} {hv} l {hh} {-hv}",
    "ᚪ": f"M 0 {H} L 0 0 l {hh} {hv} l {hh} {-hv} M 0 {hv} l {hh} {hv}",
    "ᚫ": f"M 0 {H} L 0 0 l {h} {v} M 0 {hv} l {h} {v}",
    "ᚬ": f"M {hh} 0 l 0 {H} M 0 {H/2-v*0.75} l {h} {v} m {-h} {-hv} l {h} {v}",
    "ᚭ": f"M {hh} 0 l 0 {H} M {hh} {H/2-hv} l {hh} {hv} m {-hh} 0 l {hh} {hv}",
    "ᚮ": f"M {hh} 0 l 0 {H} M {hh} {H/2-hv} l {-hh} {hv} m {hh} 0 l {-hh} {hv}",
    "ᚯ": f"M {hh} 0 l 0 {H} M {h} {H/2-v*0.75} l {-h} {v} m {h} {-hv} l {-h} {v}",
    "ᚰ": f"M {hh} 0 l 0 {H} M {h} {H/2-v*0.75} l {-h} {v} m {hh} 0 l {-hh} {hv}",
    "ᚱ": homoglyph("R"),
    "ᚲ": homoglyph("<"),
    "ᚳ": f"M 0 0 l 0 {H} M 0 {b} l {h} {v} L {h} {H}",
    "ᚴ": f"M 0 0 l 0 {H} M {h} 0 l {-h} {v}",
    "ᚵ": f"M 0 0 l 0 {H} M {h} 0 l {-h} {v} M {h/3} 0 l 0 {qv}",
    "ᚶ": f"M 0 0 l 0 {H} M {h} 0 l {-h} {v} M {-qh} {v*0.75} l {hh} {hv}",
    "ᚷ": f"M 0 {H} l {h*2} {-v*2} M {W} {H} l {-h*2} {-v*2}",
    "ᚸ": f"M 0 {H} l {h*2} {-v*2} M {W} {H} l {-h*2} {-v*2} M {hh} {H-hv} l {-hh} {-hv} l {hh} {-hv} m {h} 0 l {hh} {hv} l {-hh} {hv}",
    "ᚹ": f"M 0 {H} l 0 {-H} l {h} {v} l {-h} {v}",
    "ᚺ": f"M 0 0 l 0 {H} m {h} 0 l 0 {-H} M 0 {H/2-hv} l {h} {v}",
    "ᚻ": f"M 0 0 l 0 {H} m {h} 0 l 0 {-H} M 0 {H/2-v} l {h} {v} m {-h} 0 l {h} {v}",
    "ᚼ": f"M {hh} 0 l 0 {H} M 0 {H/2-hv} l {h} {v} m {-h} 0 l {h} {-v}",
    "ᚽ": f"M {hh} 0 l 0 {H} M {qh} {H/2-qv} l {hh} {hv} m {-hh} 0 l {hh} {-hv}",
    "ᚾ": f"M {hh} 0 l 0 {H} M 0 {H/2-hv} l {h} {v}",
    "ᚿ": f"M {hh} 0 l 0 {H} M {hh} {H/2} l {hh} {hv}",
    "ᛀ": f"M {hh} 0 l 0 {H} M 0 {H/2-hv} l {h} {v} m {-h*0.75} {-qv} l {hh} {-hv}",
    "ᛁ": homoglyph("I"),
    "ᛂ": homoglyph("ᚽ"),
    "ᛃ": f"M {hh} {H/2-v*0.75} l {-hh} {hv} l {hh} {hv} m 0 {-hv} l {hh} {hv} l {-hh} {hv}",
    "ᛄ": f"M {hh} 0 l 0 {H} M {hh} {H/2-hv} l {hh} {hv} l {-hh} {hv} l {-hh} {-hv} l {hh} {-hv}",
    "ᛅ": f"M {hh} 0 l 0 {H} M 0 {H/2+hv} l {h} {-v}",
    "ᛆ": f"M {hh} 0 l 0 {H} M 0 {H/2+hv} l {hh} {-hv}",
    "ᛇ": f"M 0 {H-hv} l {hh} {hv} l 0 {-H} l {hh} {hv}",
    "ᛈ": f"M {h} 0 l {-hh} {hv} l {-hh} {-hv} l 0 {H} l {hh} {-hv} l {hh} {hv}",
    "ᛉ": f"M {hh} 0 l 0 {H} M 0 0 l {hh} {hv} l {hh} {-hv}",
    "ᛊ": f"M {qh*3} {H} l {-hh} {-hv} l {hh} {-hv} l {-hh} {-hv} l {hh} {-hv}",
    "ᛋ": f"M 0 0 L 0 {H/2+hv} l {h} {-v} L {h} {H}",
    "ᛌ": f"M {hh} 0 l 0 {H/2}",
    "ᛍ": f"M {hh} 0 l 0 {H/2} m {-qh} {-qv} l {hh} {hv} m 0 {-hv} l {-hh} {hv}",
    "ᛎ": f"M {hh} 0 l 0 {H/2} m {-hh} {-hv} l {hh} {hv} l {hh} {-hv}",
    "ᛏ": f"M {hh} 0 l 0 {H} M 0 {hv} l {hh} {-hv} l {hh} {hv}",
    "ᛐ": homoglyph("1"),
    "ᛑ": f"M {hh} {H} l 0 {-H} l {-hh} {hv} M {qh} {H/2-qv} l {hh} {hv} m 0 {-hv} l {-hh} {hv}",
    "ᛒ": homoglyph("B"),
    "ᛓ": f"M {hh} 0 l 0 {H} M {hh} {H/2-qv} l {hh} {-hv} m {-hh} {v} l {hh} {-hv}",
    "ᛔ": f"M 0 {2*v} l {h} {-v} l {-h} {-v} L 0 {H} l {h} {-v} l {-h} {-v} M {h/3} {qv*3} l {qh} {qv} M {h/3} {H-qv*3} l {qh} {-qv}",
    "ᛕ": f"M 0 0 l 0 {H} M {h} 0 l {-h} {v} M {h} {H} l {-h} {-v}",
    "ᛖ": f"M 0 {H} l 0 {-H} l {hh} {hv} l {hh} {-hv} l 0 {H}",
    "ᛗ": f"M 0 {H} l 0 {-H} l {h} {v} M {h} {H} l 0 {-H} l {-h} {v}",
    "ᛘ": f"M {hh} 0 l 0 {H} M 0 0 l 0 {d/2} l {hh} {hv} l {hh} {-hv} l 0 {-d/2}",
    "ᛙ": f"M {hh} {H} L {hh} {hv} m {-qh} {-qv} l {hh} {hv} m 0 {-hv} l {-hh} {hv}",
    "ᛚ": f"M {hh} {H} l 0 {-H} l {hh} {hv}",
    "ᛛ": f"M {hh} {H} l 0 {-H} l {hh} {hv} M {qh} {H/2-qv} l {hh} {hv} m 0 {-hv} l {-hh} {hv}",
    "ᛜ": f"M {hh} {H/2-hv} l {hh} {hv} l {-hh} {hv} l {-hh} {-hv} z",
    "ᛝ": f"M 0 {H} l {h} {-v} l {-h} {-v} m {h} 0 l {-h} {v} l {h} {v}",
    "ᛞ": f"M 0 {H} l {h*2} {-v*2} l 0 {v*2} l {-h*2} {-v*2} z",
    "ᛟ": f"M 0 {H} l {h} {-v} l {-hh} {-hv} l {-hh} {hv} l {h} {v}",
    "ᛠ": f"M {hh} 0 l 0 {H} M 0 0 l {qh} {qv} l {qh} {-qv} l {qh} {qv} l {qh} {-qv}",
    "ᛡ": homoglyph("ᚼ"),
    "ᛢ": f"M 0 {H} l {qh} {-qv} l {qh} {qv} l 0 {-H} l {qh} {qv} l {qh} {-qv}",
    "ᛣ": f"M 0 {H} l {hh} {-hv} l {hh} {hv} M {hh} 0 l 0 {H}",
    "ᛤ": f"M 0 {H} l {h*2} {-v*2} M {W} {H} l {-h*2} {-v*2} M {hh} {H-hv} l {-hh} {-hv} l {hh} {-hv} m {h} 0 l {hh} {hv} l {-hh} {hv} M {h} {H} l 0 {-v*2}",
    "ᛥ": f"M 0 0 l 0 {H} l {h} {-v} m 0 {v} l {-h} {-v} M {h} {H} l 0 {-H} l {-h} {v} m 0 {-v} l {h} {v}",
    "ᛦ": f"M 0 {H} l 0 {-d/2} l {hh} {-hv} l {hh} {hv} l 0 {d/2} M {hh} 0 l 0 {H}",
    "ᛧ": f"M {hh} {H/2} l 0 {H/2}",
    "ᛨ": f"M 0 {hv} l {hh} {-hv} l {hh} {hv} M {hh} 0 l 0 {H} M 0 {H-hv} l {hh} {hv} l {hh} {-hv}",
    "ᛩ": f"M {h} {H} l 0 {-H} l {-h} {v} l {h} {v}",
    "ᛪ": f"M 0 0 L 0 {H/2+hv} l {h} {-v} L {h} {H} M {-qh} {hv} l {hh} {-hv} M {h+qh} {H-hv} l {-hh} {hv}",
    "᛫": homoglyph("ᛜ"),
    "᛬": homoglyph(":"),
    "᛭": homoglyph("+"),
    "ᛮ": f"M {hh} {H} l 0 {-H} l {hh} {hv} M {qh} {H/2+qv} l {hh} {-hv}",
    "ᛯ": f"M 0 0 l 0 {d/2} l {hh} {hv} l {hh} {-hv} l 0 {-d/2} M 0 {H} l 0 {-d/2} l {hh} {-hv} l {hh} {hv} l 0 {d/2} M {hh} 0 l 0 {H}",
    "ᛰ": f"M {hh} 0 l 0 {H} M {hh} {(H-v-d)/2} l {hh} {hv} l 0 {d/2} l {-hh} {hv} l {-hh} {-hv} l 0 {-d/2} l {hh} {-hv}",
    "ᛱ": f"M 0 0 l 0 {H} M 0 {b+hv} l {hh} {-hv} l {hh} {hv} L {h} {H}",
    "ᛲ": f"M 0 {H} L 0 {H/2-hv} l {h} {v} L {h} 0",
    "ᛳ": f"M {hh} {H} l 0 {-H} l {hh} {hv} l {-hh} {hv} l {-hh} {-hv} l {hh} {-hv}",
    "ᛴ": f"M 0 0 L 0 {H/2+qv*3} m {-qh} 0 l {hh*3} {-hv*3} m {-qh} 0 L {h} {H}",
    "ᛵ": f"M {qh*3} {H} l {-hh} {-hv} l {hh} {-hv} l {-hh} {-hv} l {hh} {-hv} l {-hh} {-hv} l {hh} {-hv}",
    "ᛶ": f"M {W} {H} l {-h*2} {-v*2} M {hh} {H/2+v} l {h} {-v}",
    "ᛷ": f"M 0 0 l 0 {H} M {h} {H} l 0 {-d} l {-h} {-v}",
    "ᛸ": f"M {hh} 0 l 0 {H-hv} M 0 {H} l {hh} {-hv} l {hh} {hv}",
    "€": f"M {h} {hv} l {-hh} {-hv} l {-hh} {hv} L 0 {H-hv} l {hh} {hv} l {hh} {-hv} M {-qh} {H/2+hv} l {hh} {-hv} m 0 {-hv} l {-hh} {hv}",
    "∞": f"M {h} {(H+b)/2} l {hh} {hv} l {hh} {-hv} l {-hh} {-hv} l {-h} {v} l {-hh} {-hv} l {hh} {-hv} Z",
    "≠": f"M 0 {H/2-qv} l {h} 0 m 0 {hv} l {-h} 0 M 0 {H/2+hv} l {h} {-v}",
    "♥": f"M 0 {hv} l {hh} {-hv} l {h} {v} l {-hh} {hv} l {-hh} {-hv} l {h} {-v} l {hh} {hv} L {W} {H-v} l {-h} {v} l {-h} {-v} Z",
}

# Detect homoglyphs that aren't deduplicated via #HOMOGLYPH
for rune_a, rune_b in itertools.combinations(runes.keys(), 2):
    rune_a_content = runes[rune_a]
    rune_b_content = runes[rune_b]

    if rune_a != rune_b or rune_a.startswith("#HOMOGLYPH"):
        continue

    print(f"Homoglyph detected: {rune_a} <-> {rune_b} ")

# Copy homoglyphs
for glyph, contents in runes.items():
    if not contents.startswith("#HOMOGLYPH"):
        continue

    _, referenced_glyph = contents.split(" ")
    referenced_glyph_contents = runes[referenced_glyph]
    runes[glyph] = referenced_glyph_contents

wides = [
    "M",
    "m",
    "W",
    "w",
    "Æ",
    "æ",
    "♥",
    "∞",
    "%",
    "/",
    "\\",
    "@",
    "¨",
    "©",
    "®",
    "ᚣ",
    "ᚷ",
    "ᚸ",
    "ᛞ",
    "ᛤ",
    "ᛶ",
]

# Grab current input
input_hash = str(sorted(runes.items())) + "\n"
# The file ends with a newline, so we add that here or the comparison would
# always be false.

try:
    cached_input_hash = read_file("out/input_hash")
except FileNotFoundError:
    cached_input_hash = None

runes_are_changed = input_hash != cached_input_hash

# Process args
try:
    mode = sys.argv[1]
except IndexError:
    print("Please provide a mode (full, preview, or dry) as an argument")
    exit(1)

if mode not in ["full", "preview", "dry"]:
    print(f"Mode (argument 1) must be one of full, preview, or dry. Got {mode}.")
    exit(2)

# Assign output functions depending on mode
if mode == "dry":
    write_file = _write_file_debug
    makedirs = _makedirs_debug
    delete = _delete_debug
else:
    write_file = _write_file
    makedirs = _makedirs
    delete = _delete

# Prepare directories
delete("out/font")
delete("out/archive")
if runes_are_changed:
    delete("out/stroke")
    delete("out/path")

for weight in weights:
    makedirs(f"out/stroke/{weight}")
    makedirs(f"out/path/{weight}")

makedirs("out/font")

targets = sys.argv[2:] or runes.keys()
print(f"Building {len(targets)} runes in {len(weights)} variations: {''.join(targets)}")
written_files = []

# Process runes
if runes_are_changed:
    skipped = 0
    for rune in targets:
        if runes[rune] == "":
            skipped = skipped + 1
            continue
        is_wide = rune in wides
        viewbox_width = h * (1 + int(is_wide)) + padding_h

        for weight in weights:
            style = generate_style(weight)
            svg = generate_svg(runes[rune], style)
            filename = get_stroke_filename(rune, weight)
            write_file(filename, svg)
            written_files.append(filename)

    print(f"Wrote {len(written_files)} .svg files.")
    print(f"Skipped {skipped} blank runes.")
    delete("out/input_hash")  # We wrote new but didn't convert strokes,
    # so we remove the existing hash.
else:
    print("Input has not changed, svgs will not be overwritten.")

# Debug and preview modes are done at this point
if mode != "full":
    exit(0)

if runes_are_changed:
    # Convert strokes to paths
    # Takes a long time (minimum probably 1 minute, easily up to 10)
    print("Converting strokes to paths. This will take a while.\n")
    print("  ☕ Why not make yourself a nice cup of tea?\n")

    # Progress tracking
    file_count = len(written_files)
    padding = len(str(file_count))
    start_time = time.time()

    for i, stroke_file in enumerate(written_files, start=1):
        path_file = stroke_file.replace("stroke", "path")
        stroke_to_path(stroke_file, path_file)

        # Numeric progress
        progress = str(i).rjust(padding) + "/" + str(file_count)

        # Get ETA in seconds by measuring time since starting, getting the
        # average time spent per item, and multiplying number of remaining items
        eta = ((time.time() - start_time) / i) * (file_count - i)
        eta_min = str(int(eta // 60)).rjust(2, "0")
        eta_sec = str(int(eta % 60)).rjust(2, "0")

        print(f"{progress} | ETA: {eta_min}:{eta_sec}", end="\r", flush=True)
    print("Conversions done!        ")  # Extra spaces to override ETA
    write_file("out/input_hash", input_hash)  # Converted new runes, write hash!

# Generate font files
for weight in weights:
    font = fontforge.font()
    font.encoding = "UnicodeFull"
    font.copyright = f'© Jorin Seline {time.strftime("%Y", time.localtime())}'
    font.familyname = "Antifascist Magick Runes"
    font.fullname = f"Antifascist Magick Runes {weights[weight]}"
    font.weight = weights[weight]
    font.os2_weight = weight
    font.ascent = int(H)
    font.descent = int(b)
    font.design_size = 10
    font.autoWidth(0, 0, 1)

    # Add valid space glyph to avoid "unknown character" box on IE11
    space = font.createChar(ord(" "))
    space.width = int(h)

    for rune in runes:
        add_glyph(font, rune, get_path_filename(rune, weight))

    # Generate Files
    base_name = f"out/font/Antifascist-Magick-Runes-{weight}"
    formats = ["otf", "ttf", "svg", "woff", "woff2"]
    for f in formats:
        font.generate(".".join([base_name, f]))

# Package output
for f in os.listdir("out/font"):
    _, ext = os.path.splitext(f)
    ext = ext[1:]  # Remove '.'
    makedirs(f"out/font/{ext}")
    shutil.move(f"out/font/{f}", f"out/font/{ext}/{f}")

makedirs("out/archive")

# Temporarily copy for archiving
shutil.copy("LICENSE", "out/font/LICENSE")
for f in ["zip", "tar", "gztar", "bztar", "xztar"]:
    try:
        shutil.make_archive("out/archive/Antifascist-Magick-Runes", f, "out/font")
    except Exception as e:
        print(f"Could not compile {f} archive, reason: {e}")

delete("out/font/LICENSE")  # This is now excess.

# Render previews and README images with Inkscape
makedirs("img/svg")
makedirs("img/png")
write_file("img/svg/logo.svg", generate_logo())
write_file("img/svg/alphabet.svg", generate_alphabet_preview())
write_file("img/svg/preview.svg", generate_full_preview())
write_file("img/svg/footer.svg", generate_footer())

for f in (
    "img/svg/logo.svg",
    "img/svg/alphabet.svg",
    "img/svg/preview.svg",
    "img/svg/footer.svg",
):
    rasterise_svg(f)

![Alphabet preview](img/png/alphabet.png "Sphinx of black quartz, judge my vow")

A geometric rune-inspired font with the intent of re-appropriating rune
scripture from fascists.

# Download
Go to the [releases page](https://gitlab.com/jorins/runes/-/releases) and download
any of the font files.

# Full preview
Available [here.](img/png/preview.png)

# Contributions
Always welcome! Suggestions, feedback, and issues can be filed in the [GitLab
repository issues](https://gitlab.com/jorins/runes/issues). Contributions can
be made with [merge requests](https://gitlab.com/jorins/runes/merge_requests).

Note that the code is a single file, 700-something lines long. It's not very
clean. As much as I would love to re-work it into something more maintainable,
I don't expect I'll be putting in the work for that anytime soon.

# Design concepts
The design is partially based around the idea of stone carving. It adheres to a
few principles that would probably make this easier, although I know nothing
about stone carving so this should be taken with a grain of salt. Nevertheless,
it makes for a simple style.

* All runes are made with two or three columns of equally spaced lines. These
  may be subdivided once.
* Connecting these lines are angular lines at an exact angle of your choosing.
  Either 45 degrees or 30 degrees will look the simplest. There are no
  horizontal lines, with a few exceptions for characters like minus and
  brackets
* Capital runes should have an eye catching element. Lower-case runes should
  instead aim for legibility. As such, runes may be a bit less traditional
  looking.
* Runes should be visually balanced, to contrast themselves from fascist
  appropriation which often skews them for (literally) edgy styling.
* Similarly, thin strokes and rounded corners and caps are preferable.

# Building
Before building, you will need Python, Inkscape, and Fontforge with Python
support. You must be able to `import fontforge` in a stand-alone Python script
and you must be able to call `inkscape` from the command line. Your Inkscape
version should support command line actions. It should work starting with
Inkscape 1.1, but it has only been tested with 1.3.

1. Modify `runes.py` if you wish to change anything.
2. Generate stroked paths of the font with `runes.py preview`
3. Preview the runes in the `out/stroke` folder.
4. Render using `runes.py full`. This will take a while as the script has to
   convert strokes to paths for all runes across all weights (~2106 files) in
   Inkscape. It takes about seven minutes on my desktop.
5. Output files are created, fonts being placed in `out/font` and pre-packed
   archives in `out/archive`.

You can also run the script in debug mode with `runes.py debug`. This will
prevent it from writing to disk and will exit before the script begins to call
Inkscape.

# License
A `LICENSE` file should be available in the repository or your downloaded
archive. It is a non-standard copyleft license and part of the artistic intent
of the project.

![The only good Nazi is one that is dead](img/png/footer.png "The only good Nazi is one that is dead")

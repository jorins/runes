# v1.2

Released 2023-09-10

## Project changes

* Update license
* Update documentation

## Font changes

* Replace the stick-based numerals with arabic numerals
* Fix `«`. Was duplicate of `<`, now more accurate
* Fix `»`. Was duplicate of `>`, now more accurate
* Aligned `&` correctly with baseline

## Code changes

* Implement a homoglyph system to de-duplicate rune path definitions
* Remove automatic installation after compiling
* Separate stroke-based and path-based vector file stages
* Switch from Inkscape's old verb system to the new action system
* Lots of cleanup and clarifications

# v1.1

Released 2020-02-24

First release, meeting 1.0 and 1.1 milestones.

# v0.1

Released 2020-01-01

Pre-release, incomplete
